# Medicare Advantage Enrollment

This table is updated monthly and relies upon two different sources of data:

CMS [State/County/Contract](https://www.cms.gov/Research-Statistics-Data-and-Systems/Statistics-Trends-and-Reports/MCRAdvPartDEnrolData/Monthly-MA-Enrollment-by-State-County-Contract) and the alt [CPSC](https://www.cms.gov/research-statistics-data-and-systemsstatistics-trends-and-reportsmcradvpartdenroldatamonthly/monthly-enrollment-cpsc) files need to be extracted and the files inside must be assigned pointers for the script.

This report is critical for operations in several departments and must be maintained regularly.

The CMS data are scheduled to be updated on the 15th of every month.

The script will check it is the 18th of every month before executing the rest of the script.

When run between the first and seventeenth, the previous months data are downloaded.


# Instructions

Specify path to folder where the data are downloaded
	
You will be prompted for a file name via dialog to save the output table
	
	
### Additional considerations

Set the path for saving the raw data on AWS instead of passing in a dir
	
Set the publication location ([Currently here](https://drive.google.com/drive/folders/1kn6ScAF6cCt5BFvTrz-3Ogy1EkrRqY35?usp=sharing)) as the output destination instead of the saveasfile routine
	
Set to run on a specific day each month
	
Add a routine to email specific people with a link to the latest file when successfully run


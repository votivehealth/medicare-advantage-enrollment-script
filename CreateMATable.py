# The CMS data are updated on the 15th of every month or so. 
# We will check it is the 18th of every month before executing the rest of the script



def main():

    def convertmonth(month):
        ''' this function converts the month numeric value as a written string. it is necessary for creating the zipfile download link'''
        # dictionary for insertion into the zip download link
        monthdict = {
            '01' : 'january',
            '02' : 'february',
            '03' : 'march',
            '04' : 'april',
            '05' : 'may',
            '06' : 'june',
            '07' : 'july',
            '08' : 'august',
            '09' : 'september',
            '10' : 'october',
            '11' : 'november',
            '12' : 'december'
        }
        stringMonth = monthdict[month]
        return stringMonth


    def downloadMAData():
        '''this function downloads the CMS data needed to create the Medicare Advantage Enrollment Table
        If the date it is run is 1-15 it will get the previous months files because the current month's are not published
        Tested for January case - rolling back to previous year's data '''
        import datetime
        import zipfile
        import requests
        from zipfile import ZipFile
        import tkinter
        from tkinter import filedialog

        # get path via dialog to the dir where data are saved, set initialdir!
        tkObj = tkinter.Tk()
        path = tkinter.filedialog.askdirectory(parent=tkObj, title="Pick directory to save source data", initialdir='C:\\Users\\')
        tkObj.destroy()

        today = datetime.date.today()
        currentmonth = today.month
        currentyear = today.year
        currentday = today.day
        if currentday <= 14:
            print('getting previous month\'s data')
            # pad single digit months with zero
            if len(str(currentmonth)) == 1:
                paddedmonth = '0' + str(currentmonth -1)
            else:
                paddedmonth = str(currentmonth)
            # january presents an issue, must also roll year back
            if paddedmonth == '01':
                suffix = str(currentyear - 1) + '_12'
            else:
                # create the YYYY_MM suffix for the csv files
                suffix = str(currentyear) + '_' + str(paddedmonth)
        elif currentday > 14:
            # pad single digit months with zero
            if len(str(currentmonth)) == 1:
                paddedmonth = '0' + str(currentmonth)
            else:
                paddedmonth = str(currentmonth)
            # create the YYYY_MM suffix for the csv files
            suffix = str(currentyear) + '_' + str(paddedmonth)
            

        # build the download urls:
        scczipurl = 'https://www.cms.gov/files/zip/ma-enrollment-statecountycontract-{}-{}-abridged-version-exclude-rows-10-or-less-enrollees.zip'.format(convertmonth(paddedmonth), currentyear)
        cpsczipurl = 'https://www.cms.gov/files/zip/monthly-enrollment-cpsc-{}-{}.zip'.format(convertmonth(paddedmonth), currentyear)
        # download zips (to our s3 bucket for archival storage??)
        r1 = requests.get(scczipurl)
        r2 = requests.get(cpsczipurl)
        scczip = path + '/SCC_Enrollment_MA_Alt_{}.zip'.format(suffix)
        cpsczip = path + '/CPSC_Enrollment_MA_{}.zip'.format(suffix)
        with open(scczip, 'wb') as output:
            output.write(r1.content)
        with open(cpsczip, 'wb') as output:
            output.write(r2.content)
        print('zips written to file')
        #extract the files for processing
        listofzips = [scczip, cpsczip]
        for x in listofzips:
            with ZipFile(x, 'r') as zipObj:
                zipObj.extractall(path)
        # function call to join the tables
        maTableJoin(path, suffix)


    def maTableJoin(datadir, suffix):

        '''Function is passed directory containing the Medicare Advantage enrollment files for a given month.
        Path to the zipped files is passed in, DataFrames are created from the csvs and a dialog window allows the user
        to specify the outfile name and location'''

        import pandas as pd
        import os
        import tkinter
        from tkinter import filedialog

        #  walk the datadir and assign pointers to the files needed
        for root, dirs, files in os.walk(datadir):
            for name in files:
                if "CPSC_Contract_Info" in name:
                    concsv = root + '\\' + name
                if "SCC_Enrollment_MA_Alt" in name:
                    enrcsv = root + '\\' + name

        #  Read SCC alt table into dataframe, convert FIPS to integers (stored as text in original excel file)
        df1 = pd.read_csv(enrcsv, usecols=['State', 'County', 'Contract ID', 'Organization Name', 'Organization Type', 
                                           'Plan Type', 'FIPS Code', 'Enrolled'], 
                                           converters={'FIPS Code': '{:0>5}'.format})

        # contract info needed for joining parent organization to SCC table - use the same month and year as the SCC table!
        condf = pd.read_csv(concsv, encoding='iso-8859-1', usecols=['Contract ID', 'Organization Name', 'Organization Marketing Name', 'Plan Name', 'Parent Organization'])
        condf.drop_duplicates(keep=False)


        # write Parent organization from the contract_info table to dict
        parent = condf[['Contract ID', 'Parent Organization']]
        # empty dictionary to store values
        parentdict = {}
        for x, y in zip(parent['Contract ID'], parent['Parent Organization']):
            parentdict[x] = y

        # write plan name from the contract_info table to dict
        marketing = condf[['Contract ID', 'Organization Marketing Name']]
        marketdict = {}
        for x, y in zip(marketing['Contract ID'], marketing['Organization Marketing Name']):
            marketdict[x] = y

        # write plan name from the contract_info table to dict
        plans = condf[['Contract ID', 'Plan Name']]
        plandict = {}
        for x, y in zip(plans['Contract ID'], plans['Plan Name']):
            plandict[x] = y

        # Join the values to the SCC dataframe:
        df1.loc[:,['Parent Organization']] = df1['Contract ID'].map(parentdict)
        df1.loc[:,['Plan Name']] = df1['Contract ID'].map(plandict)
        df1.loc[:,['Organization Marketing Name']] = df1['Contract ID'].map(marketdict)

        # set column names to snake case (lower with underscores)
        df1.columns = df1.columns.str.lower()
        df1.columns = df1.columns.str.replace(" ", "_")
        
        # replacements to format the parent_organization field 
        df1['parent_organization'] = df1['parent_organization'].replace('Blue Cross Blue Shield of Michigan Mutual Ins. Co.', 'Blue Cross Blue Shield of Michigan') #BCBS MI
        df1['parent_organization'] = df1['parent_organization'].replace('UnitedHealth Group, Inc.', 'United Healthcare')
        df1['parent_organization'] = df1['parent_organization'].replace('CVS Health Corporation', 'Aetna')
        df1['parent_organization'] = df1['parent_organization'].replace('Humana Inc.', 'Humana')
        df1['parent_organization'] = df1['parent_organization'].replace('Anthem Inc.', 'Anthem')
        df1['parent_organization'] = df1['parent_organization'].replace('CIGNA', 'Cigna')
        df1['parent_organization'] = df1['parent_organization'].replace('Essence Group Holdings Corporation', 'Essence Healthcare')
        df1['parent_organization'] = df1['parent_organization'].replace('University of Wisconsin Hospitals and Clincs Autho', 'University of Wisconsin Hospitals and Clinics')
        df1['parent_organization'] = df1['parent_organization'].replace('PIEDMONT HEALTH SERVICES, INC.', 'Piedmont Health Services')
        df1['parent_organization'] = df1['parent_organization'].replace('Lifetime Healthcare, Inc.', 'Lifetime Healthcare')
        df1['parent_organization'] = df1['parent_organization'].replace('MEDICAL MUTUAL OF OHIO', 'Medical Mutual of Ohio')
        df1['parent_organization'] = df1['parent_organization'].replace('Molina Healthcare, Inc.,', 'Molina Healthcare')
        df1['parent_organization'] = df1['parent_organization'].replace('Medica Holding Company', 'Medica')
        df1['parent_organization'] = df1['parent_organization'].replace('HealthPartners, Inc.', 'HealthPartners')
        df1['parent_organization'] = df1['parent_organization'].replace(' Mutual Holding', '') #Guidewell
        df1['parent_organization'] = df1['parent_organization'].replace('Kaiser Foundation Health Plan, Inc', 'Kaiser') #kaiser
        df1['parent_organization'] = df1['parent_organization'].replace('Triton Health Systems, L.L.C.', 'Triton Health')
        df1['parent_organization'] = df1['parent_organization'].replace('Associated Care Ventures, Inc.', 'Associated Care')
        df1['parent_organization'] = df1['parent_organization'].replace('EXPERIENCE HEALTH, INC.', 'Experience Health')
        df1['parent_organization'] = df1['parent_organization'].replace('Capital District Physicians\' Health Plan, Inc.', 'Capital District Physicians\' Health Plan')
        df1['parent_organization'] = df1['parent_organization'].replace('Centene Corporation', 'Centene') 
        df1['parent_organization'] = df1['parent_organization'].replace(to_replace=r' Corp\.?$', value='', regex=True)
        df1['parent_organization'] = df1['parent_organization'].replace(to_replace=r',? Incorp\.?$', value='', regex=True)
        df1['parent_organization'] = df1['parent_organization'].replace(to_replace=r',? LLC\.?$', value='', regex=True)
        df1['parent_organization'] = df1['parent_organization'].replace(to_replace=r',? Inc\.?$', value='', regex=True)
        df1['parent_organization'] = df1['parent_organization'].replace(', LLC', '')
        df1['parent_organization'] = df1['parent_organization'].replace(' LLC', '')
        df1['parent_organization'] = df1['parent_organization'].replace(', L.L.C.', '')
        df1['parent_organization'] = df1['parent_organization'].replace(' LLC.', '')
        df1['parent_organization'] = df1['parent_organization'].replace(' Inc.', '')
        df1['parent_organization'] = df1['parent_organization'].replace(', Inc', '')
        df1['parent_organization'] = df1['parent_organization'].replace(' Inc', '')
        df1['parent_organization'] = df1['parent_organization'].replace(to_replace=r', P\.C\.$', value='', regex=True)
        df1['parent_organization'] = df1['parent_organization'].replace(' (SHC)', '')
        df1['parent_organization'] = df1['parent_organization'].replace(' Autho', '')
        df1 = df1.replace('Medicare y Mucho MÃ¡s', 'Medicare y Mucho Mas')
        
        # Write the county level data to file
        tkObj = tkinter.Tk()
        outxl = tkinter.filedialog.asksaveasfilename(title='Please name the file and where to save it!', parent=tkObj, initialfile='SCCMAEnrollment{}.xlsx'.format(suffix))
        with open(outxl, 'wb') as writer:
            df1.to_excel(writer, index=False)
        tkObj.destroy()
        
        
        print('The Medicare Advantage table was successfully created')
        

    # execute the script
    downloadMAData()





# specify path to folder where the data are downloaded
# you will be prompted for a file name via dialog to save the output
if __name__ == '__main__':
    main()